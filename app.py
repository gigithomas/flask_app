from flask import Flask, render_template, request
from flask_cors import CORS
import config
import json
import datetime
from urllib.request import urlopen
from flask import make_response
from functools import wraps, update_wrapper
import pygsheets
import pandas as pd
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from oauth2client.contrib import gce
from apiclient.http import MediaFileUpload
# You may need to restart your runtime prior to this, to let your installation take effect
# Some basic setup:
# Setup detectron2 logger
import detectron2
from detectron2.utils.logger import setup_logger
import matplotlib
# import some common libraries
import numpy as np
import cv2
import random
from google.colab.patches import cv2_imshow
import string


# import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog

import os
import numpy as np
import json
from detectron2.structures import BoxMode
import static.util.gender_age_prediction_v3 as gender_age_model
import static.util.color_detector as color_model
import static.util.color as color

app = Flask(__name__)
CORS(app)

app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

CLIENT_SECRET = "static/data/mytestproject-1487674567360-0e71f57657f4.json"
PARENT_FOLDER = "1pb7-qRXD4Av-NgtabjrWDR9TA61QKzZ_"

SCOPES = 'https://www.googleapis.com/auth/drive', 'https://www.googleapis.com/auth/spreadsheets'
store = file.Storage('static/temp.json')
##creds = store.get()

##if not creds or creds.invalid:
    ##flow = client.flow_from_clientsecrets(CLIENT_SECRET, SCOPES)
    ##creds = tools.run_flow(flow, store)
##SERVICE = build('drive', 'v3', http=creds.authorize(Http()))
##SS_SERVICE = build('sheets', 'v4', http=creds.authorize(Http()))


# ---------------------------------------
# GDrive API: Create New Folder
# ---------------------------------------
##def createGDriveFolder(filename, parent):
  ##  file_metadata = {'name': filename, 'parents': [parent],
    ##                 'mimeType': "application/vnd.google-apps.folder"}

    ##folder = SERVICE.files().create(body=file_metadata,
    ##                                fields='id').execute()
    ##print('Upload Success!')
    ##print('FolderID:', folder.get('id'))
    ##return folder.get('id')


# ------------------------------------
# GDrive API: Check if Filename exists
# ------------------------------------
##def fileInGDrive(filename):
  ##  results = SERVICE.files().list(
    ##    q="mimeType='application/vnd.google-apps.spreadsheet' and name='" + filename + "' and trashed = false and "
      ##                                                                                 "parents in '" + PARENT_FOLDER
        ##  + "'",
        ##fields="nextPageToken, files(id, name)").execute()
    ##items = results.get('files', [])
    ##if items:
      ##  return True
    ##else:
      ##  return False


##def fileId(filename):
  ##  results = SERVICE.files().list(
    ##    q="mimeType='application/vnd.google-apps.spreadsheet' and name='" + filename + "' and trashed = false and "
      ##                                                                                 "parents in '" + PARENT_FOLDER
        ##  + "'",
        ##fields="nextPageToken, files(id, name)").execute()
   ## items = results.get('files', [])
    ##if items:
      ##  return items[0]['id']
 ##   else:
   ##     return False


# ------------------------------------
# GDrive API: Check if Folder exists
# ------------------------------------
##def folderInGDrive(filename):
  ##  results = SERVICE.files().list(
    ##    q="mimeType='application/vnd.google-apps.folder' and name='" + filename + "' and trashed = false and parents "
      ##                                                                            "in '" + PARENT_FOLDER + "'",
        ##fields="nextPageToken, files(id, name)").execute()
   ## items = results.get('files', [])
    ##if items:
      ##  return True
   ## else:
     ##   return False


# ---------------------------------------
# GDrive API: Upload files to Google Drive
# ---------------------------------------
##def writeToGDrive(filename, source, folder_id, mimetype='application/vnd.ms-excel'):
  ##  file_metadata = {'name': filename, 'parents': [folder_id],
    ##                 'mimeType': 'application/vnd.google-apps.spreadsheet'}
    ##media = MediaFileUpload(source,
      ##                      mimetype=mimetype)

    ##if fileInGDrive(filename) is False:
      ##  file = SERVICE.files().create(body=file_metadata,
        ##                              media_body=media,
          ##                            ).execute()
        ##print('Upload Success!')
        ##print('File ID:', file.get('id'))
       ## return file.get('id')

    ##else:
      ##  fileId(filename)
       ## file = SERVICE.files().update(body=file_metadata,
         ##                             media_body=media,
           ##                           fields='id',
             ##                         fileId=fileId(filename)).execute()
        ##print('Upload Success!')
        ##print('File ID:', file.get('id'))
        ##return file.get('id')


def nocache(view):
    @wraps(view)
    def no_cache(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Last-Modified'] = datetime.datetime.now()
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response

    return update_wrapper(no_cache, view)


def get_balloon_dicts(img_dir):
    if img_dir.split("/")[-1] == "train":
        json_file = "/home/textmercato/filestore/deepfashion_detectron/training_data.json"
    else:
        json_file = "/home/textmercato/filestore/deepfashion_detectron/valid_data.json"
    with open(json_file) as f:
        dataset_dicts = json.load(f)
    return dataset_dicts


from detectron2.data import DatasetCatalog, MetadataCatalog

# all_classes = ["uncategorized", "short sleeve top", "long sleeve top", "short sleeve outwear",
#                "long sleeve outwear", "vest", "sling", "shorts", "trousers", "skirt", "short sleeve dress",
#                "long sleeve dress", "vest dress", "sling dress", 'bag', 'belt', 'boots', 'footwear',
#                'sunglasses', 'headwear', 'scarf/tie']
# all_classes = ["uncategorized", "short sleeve top", "long sleeve top", "short sleeve outwear",
#                "long sleeve outwear", "vest", "sling", "shorts", "trousers", "skirt", "short sleeve dress",
#                "long sleeve dress", "vest dress", "sling dress"]
classMapping = {
    "shirts": 0,
    "saree": 1,
    "brief": 2,
    "vest": 3,
    "boxer": 4,
    "bra": 5,
    "tshirts": 6,
    "skirts": 7,
    "shorts": 8,
    "dress": 9,
    # "trousers": "trousers",
    "tunics": 10,
    "kurtas": 11,
    "jumpsuit": 12,
    "suits": 13,
    "sweatshirts": 14,
    "jackets": 15,
    "jeans": 16,
    "pyjamas": 17,
    "cargo": 18,
    "track pant": 19,
    "formal trouser": 20,
    "casual trouser": 21
}

# classMapping = {"tops": 0, "saree": 1, "bottom-innerwear": 2, "top-innerwear": 3,
#                 "shorts": 4, "dress": 5, "trousers": 6, "skirts": 7}

# all_classes = ["tshirts", "shirts", "shorts", "skirts", "dress", "saree", "boxers", "briefs", "bra", "vest",
# "trousers"]
all_classes = list(classMapping.keys())
print(all_classes)

all_classes_dict = {}
for i in range(len(all_classes)):
    all_classes_dict[i] = all_classes[i]
for d in ["train", "val"]:
    DatasetCatalog.register("deepfashion_" + d, lambda d=d: get_balloon_dicts("deepfashion/" + d))
    MetadataCatalog.get("deepfashion_" + d).set(
        thing_classes=all_classes)
deepfashion_metadata = MetadataCatalog.get("deepfashion_train")

# Training
from detectron2.engine import DefaultTrainer
from detectron2.config import get_cfg

cfg = get_cfg()
cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
cfg.DATASETS.TRAIN = ("deepfashion_train",)
cfg.DATASETS.TEST = ()
cfg.DATALOADER.NUM_WORKERS = 5 #2
cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(
    "COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")  # Let training initialize from model zoo
cfg.SOLVER.IMS_PER_BATCH = 2
cfg.SOLVER.BASE_LR = 0.00025  # pick a good LR
cfg.SOLVER.MAX_ITER = 221007 * 20  # 300 iterations seems good enough for this toy dataset; you may need to train
# longer for a practical dataset
cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 512  # faster, and good enough for this toy dataset (default: 512)
cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(list(classMapping.keys()))  # only has one class (ballon)
from detectron2.utils.visualizer import ColorMode

cfg.OUTPUT_DIR
# dataset_dicts = get_balloon_dicts("deepfashion/val")
# model_0144999 model_0019999
cfg.MODEL.WEIGHTS = os.path.join("/home/textmercato/filestore/deepfashion_detectron/outputs_tm_v1_upgrade_2/model_0049999"
                                 ".pth")
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.65
cfg.DATASETS.TEST = ("deepfashion_val",)
predictor = DefaultPredictor(cfg)


def get_feature(image_path):
    im = cv2.imread(image_path)
    outputs = predictor(im)
    v = Visualizer(im[:, :, ::-1],
                   metadata=deepfashion_metadata,
                   scale=0.8,
                   instance_mode=ColorMode.IMAGE_BW  # remove the colors of unsegmented pixels
                   )
    v = v.draw_instance_predictions(outputs["instances"].to("cpu"))
    output_image_path = 'static/output_image.jpg'
    cv2.imwrite(output_image_path, v.get_image()[:, :, ::-1])

    instances = outputs["instances"].to("cpu")
    boxes = instances.__dict__["_fields"]["pred_boxes"].tensor.numpy()
    classes = instances.__dict__["_fields"]["pred_classes"].numpy()
    print(classes)
    classes = [all_classes_dict[int(i)] for i in classes]
    masks = instances.__dict__["_fields"]["pred_masks"].numpy()
    scores = instances.__dict__["_fields"]["scores"].numpy()

    return output_image_path, {"boxes": boxes, "classes": classes, "masks": masks, "scores": scores, "im": im}


@app.route('/_ah/health')
def hello_world():
    return 'I am alive!'


@app.route("/", methods=['GET'])
def load_index():
    response = {}
    return render_template('index.html', result=response)


@app.route("/segmentation", methods=['POST', 'GET'])
@nocache
def extract_feature():
    start_time = datetime.datetime.now()

    if request.method == 'POST':
        #image_url = request.form['image_url']
        image_file = urlopen(image_url)
        input_image_path = 'static/input_image.jpg'
        with open(input_image_path, 'wb') as output:
            output.write(image_file.read())
        output_image_path, raw_output = get_feature(input_image_path)
        end_time = datetime.datetime.now()
        duration = end_time - start_time
        response = {"input_image_path": "../" + input_image_path,
                    "output_image_path": "../" + output_image_path, "raw_output": raw_output,
                    "time_visibility": "visible", "table_visiblity": "visible", "duration": duration}
        return render_template('segmentation.html', result=response)

    else:
        response = {"time_visibility": "hidden", "table_visiblity": "hidden", "raw_output": {"classes": []}}
        return render_template('segmentation.html', result=response)


# render_template(template_name_or_list=[""])
def get_color_classes(image_url=""):
    input_image_path = image_url
    output_image_path, raw_output = get_feature(input_image_path)
    im = raw_output["im"]
    print(raw_output["classes"])
    print(len(raw_output["masks"]))

    masked_data = {}
    # masked_data["google_tables"] =[]
    # google_tables = []
    colors = {}
    classes = raw_output["classes"]
    scores = raw_output["scores"]
    counter = 1

    # color_data = color_model.cloth_color(input_image_path)
    # color_data["class_name"] = "Full Input Image"
    # masked_data["../" + input_image_path] = color_data
    # google_tables.append(color_data["google_table"])
    # colors.append(color_data["colors"])

    for i in raw_output["boxes"]:
        # temp_masked_img = im.reshape((im.shape[0] * im.shape[1]), im.shape[2]).T * i.astype(np.uint8).reshape(
        #     (im.shape[0] * im.shape[1]))
        # temp_masked_img = temp_masked_img.T.reshape(im.shape[0], im.shape[1], im.shape[2])
        temp_masked_img = im[i[1]:i[3], i[0]:i[2], :]
        temp_masked_file_name = 'static/masked_image_' + str(counter) + ".jpg"

        # tmp = cv2.cvtColor(temp_masked_img, cv2.COLOR_BGR2GRAY)
        # _, alpha = cv2.threshold(tmp, 0, 255, cv2.THRESH_BINARY)
        # b, g, r = cv2.split(temp_masked_img)
        # rgba = [b, g, r, alpha]
        # dst = cv2.merge(rgba, 4)
        # cv2.imwrite(temp_masked_file_name, dst)

        cv2.imwrite(temp_masked_file_name, temp_masked_img)
        # masked_files.append("../"+temp_masked_file_name)
        try:
            color_data = color_model.cloth_color(temp_masked_file_name)
            colors[raw_output["classes"][counter - 1]] = color_data["colors"]
        except Exception as e:
            colors[raw_output["classes"][counter - 1]] = []
            # print(e)
            pass

        # color_data["class_name"] = raw_output["classes"][counter - 1]
        # masked_data["../" + temp_masked_file_name] = color_data
        # google_tables.append(color_data["google_table"])
        # colors.append(color_data["colors"])
        # masked_data["google_tables"].append(color_data["google_table"])
        counter += 1

    # for i in raw_output["masks"]:
    #     temp_masked_img = im.reshape((im.shape[0] * im.shape[1]), im.shape[2]).T * i.astype(np.uint8).reshape(
    #         (im.shape[0] * im.shape[1]))
    #     temp_masked_img = temp_masked_img.T.reshape(im.shape[0], im.shape[1], im.shape[2])
    #     temp_masked_file_name = 'static/masked_image_' + str(counter) + ".jpg"
    #
    #     # tmp = cv2.cvtColor(temp_masked_img, cv2.COLOR_BGR2GRAY)
    #     # _, alpha = cv2.threshold(tmp, 0, 255, cv2.THRESH_BINARY)
    #     # b, g, r = cv2.split(temp_masked_img)
    #     # rgba = [b, g, r, alpha]
    #     # dst = cv2.merge(rgba, 4)
    #     # cv2.imwrite(temp_masked_file_name, dst)
    #
    #     cv2.imwrite(temp_masked_file_name, temp_masked_img)
    #     # masked_files.append("../"+temp_masked_file_name)
    #     try:
    #         color_data = color_model.cloth_color(temp_masked_file_name)
    #         colors[raw_output["classes"][counter - 1]] = color_data["colors"]
    #     except Exception as e:
    #         colors[raw_output["classes"][counter - 1]] = []
    #         # print(e)
    #         pass
    #
    #     # color_data["class_name"] = raw_output["classes"][counter - 1]
    #     # masked_data["../" + temp_masked_file_name] = color_data
    #     # google_tables.append(color_data["google_table"])
    #     # colors.append(color_data["colors"])
    #     # masked_data["google_tables"].append(color_data["google_table"])
    #     counter += 1

    response = {"classes": classes, "classes_score": scores, "colors": colors}
    return response


@app.route("/color", methods=['POST', 'GET'])
@nocache
def extract_color(image_url=""):
    start_time = datetime.datetime.now()
    if request.method == 'GET':
        response = {"time_visibility": "hidden", "table_visiblity": "hidden", "raw_output": {},
                    "google_tables": json.dumps([]), "colors": json.dumps([])}
        return render_template('color.html', result=response)
    elif request.method == 'POST':
        image_url = request.form['image_url']
        image_file = urlopen(image_url)
        input_image_path = 'static/input_image.jpg'
        with open(input_image_path, 'wb') as output:
            output.write(image_file.read())
        output_image_path, raw_output = get_feature(input_image_path)
        im = raw_output["im"]

        masked_data = {}
        # masked_data["google_tables"] =[]
        google_tables = []
        colors = []
        counter = 1

        color_data = color_model.cloth_color(input_image_path)
        color_data["class_name"] = "Full Input Image"
        masked_data["../" + input_image_path] = color_data
        google_tables.append(color_data["google_table"])
        colors.append(color_data["colors"])
        # print(type(im), im.shape)
        #boxes
        for i in range(len(raw_output["masks"])):
            temp_masked_img = im.reshape((im.shape[0] * im.shape[1]), im.shape[2]).T * raw_output["masks"][i].astype(np.uint8).reshape(
                (im.shape[0] * im.shape[1]))
            temp_masked_img = temp_masked_img.T.reshape(im.shape[0], im.shape[1], im.shape[2])
            # print(im.shape, type(im))
            # print(i)
            # print(temp_masked_img)
            # print(temp_masked_img.shape)
            # print(raw_output["boxes"][i])
            # print(type(raw_output["boxes"][i][0]))


            # temp_masked_img = im[int(i[1]):int(i[3]), int(i[0]):int(i[2])]
            temp_masked_img = temp_masked_img[int(raw_output["boxes"][i][1]):int(raw_output["boxes"][i][3]), int(raw_output["boxes"][i][0]):int(raw_output["boxes"][i][2]), :]

            temp_masked_file_name = 'static/masked_image_' + str(counter) + ".jpg"

            # tmp = cv2.cvtColor(temp_masked_img, cv2.COLOR_BGR2GRAY)
            # _, alpha = cv2.threshold(tmp, 0, 255, cv2.THRESH_BINARY)
            # b, g, r = cv2.split(temp_masked_img)
            # rgba = [b, g, r, alpha]
            # dst = cv2.merge(rgba, 4)
            # cv2.imwrite(temp_masked_file_name, dst)

            cv2.imwrite(temp_masked_file_name, temp_masked_img)
            # masked_files.append("../"+temp_masked_file_name)

            color_data = color_model.cloth_color(temp_masked_file_name)
            color_data["class_name"] = raw_output["classes"][counter - 1]
            masked_data["../" + temp_masked_file_name] = color_data
            google_tables.append(color_data["google_table"])
            colors.append(color_data["colors"])
            # masked_data["google_tables"].append(color_data["google_table"])
            counter += 1

        # for i in raw_output["masks"]:
        #     temp_masked_img = im.reshape((im.shape[0] * im.shape[1]), im.shape[2]).T * i.astype(np.uint8).reshape(
        #         (im.shape[0] * im.shape[1]))
        #     temp_masked_img = temp_masked_img.T.reshape(im.shape[0], im.shape[1], im.shape[2])
        #     temp_masked_file_name = 'static/masked_image_' + str(counter) + ".jpg"
        #
        #     # tmp = cv2.cvtColor(temp_masked_img, cv2.COLOR_BGR2GRAY)
        #     # _, alpha = cv2.threshold(tmp, 0, 255, cv2.THRESH_BINARY)
        #     # b, g, r = cv2.split(temp_masked_img)
        #     # rgba = [b, g, r, alpha]
        #     # dst = cv2.merge(rgba, 4)
        #     # cv2.imwrite(temp_masked_file_name, dst)
        #
        #     cv2.imwrite(temp_masked_file_name, temp_masked_img)
        #     # masked_files.append("../"+temp_masked_file_name)
        #
        #     color_data = color_model.cloth_color(temp_masked_file_name)
        #     color_data["class_name"] = raw_output["classes"][counter - 1]
        #     masked_data["../" + temp_masked_file_name] = color_data
        #     google_tables.append(color_data["google_table"])
        #     colors.append(color_data["colors"])
        #     # masked_data["google_tables"].append(color_data["google_table"])
        #     counter += 1

        end_time = datetime.datetime.now()
        duration = end_time - start_time
        response = {"input_image_path": "../" + input_image_path,
                    "raw_output": masked_data,
                    "time_visibility": "visible", "table_visiblity": "visible", "duration": duration, "google_tables":
                        json.dumps(google_tables), "colors": json.dumps(colors)}
        return render_template('color.html', result=response)
    else:
        print("x")


@app.route("/gender_age", methods=['POST', 'GET'])
@nocache
def extract_gender_age():
    start_time = datetime.datetime.now()

    if request.method == 'POST':
        image_url = request.form['image_url']
        image_file = urlopen(image_url)
        input_image_path = 'static/input_gender_image.jpg'
        output_image_path = 'static/output_gender_image.jpg'

        with open(input_image_path, 'wb') as output:
            output.write(image_file.read())

        raw_output = gender_age_model.predict(input_image_path, output_image_path)
        end_time = datetime.datetime.now()
        duration = end_time - start_time
        response = {"input_image_path": "../" + input_image_path,
                    "output_image_path": "../" + output_image_path, "raw_output": raw_output,
                    "time_visibility": "visible", "table_visiblity": "visible", "duration": duration}
        return render_template('gender_age.html', result=response)

    else:
        response = {"time_visibility": "hidden", "table_visiblity": "hidden", "raw_output": {"genders": []}}
        return render_template('gender_age.html', result=response)


def get_all_for_images(image_path):
    prediction = {"image_URL": image_path, "image": '=Image("' + image_path + '")', "ages": [], "genders": [],
                  "classes_score": [],
                  "classes": [], "colors": []}
    try:
        image_url = request.form['image_url']
        image_file = urlopen(image_path)
        input_image_path = 'static/input_gender_image.jpg'
        output_image_path = 'static/output_gender_image.jpg'
        with open(input_image_path, 'wb') as output:
            output.write(image_file.read())
        try:
            gender_age_prediction = gender_age_model.predict(input_image_path, output_image_path)
            prediction["ages"] = gender_age_prediction["ages"]
            prediction["genders"] = gender_age_prediction["genders"]
            try:
                color_class_output = get_color_classes(input_image_path)
                # print(color_class_output)
                prediction["colors"] = color_class_output["colors"]
                prediction["classes"] = color_class_output["classes"]
                prediction["classes_score"] = color_class_output["classes_score"]

            except Exception as e:
                print(str(e))
                pass
        except Exception as e:
            print(e)

            pass
    except Exception as e:
        print(e)

        pass
    return prediction
print(get_all_for_images("https://assets.myntassets.com/h_1440,q_90,w_1080/v1/assets/images/2042528/2017/9/28/11506599316866-Mast--Harbour-Men-Red--Blue-Regular-Fit-Checked-Casual-Shirt-7991506599316924-1.jpg"))

def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


@app.route("/batch", methods=['POST', 'GET'])
@nocache
def batch():
    start_time = datetime.datetime.now()

    if request.method == 'POST':
        all_image_urls = request.form['image_url'].split(",")

        all_image_urls = [url.strip() for url in all_image_urls]
        # print(all_image_urls)
        results = []
        for i in all_image_urls:
            results.append(get_all_for_image(i))
        data = pd.DataFrame.from_records(results)
        filename = "catalog-ai-" + randomString(6)
        source_filename = filename + ".xlsx"
        output_file = filename
        data.to_excel(source_filename, sheet_name='Sheet1')

        file_id = writeToGDrive(filename, source_filename, "1pb7-qRXD4Av-NgtabjrWDR9TA61QKzZ_")
        file_url = 'https://docs.google.com/spreadsheets/d/' + file_id + '/edit?usp=sharing'
        csv_url = 'https://docs.google.com/spreadsheets/d/' + file_id + '/export?format=csv&id=' + file_id
        os.remove(source_filename)
        end_time = datetime.datetime.now()
        duration = end_time - start_time
        response = {"duration": duration, "time_visibility": "visible", "table_visiblity": "visible",
                    "raw_output": {"file_url": file_url, "csv_url": csv_url}}

        return render_template('batch.html', result=response)

    else:
        response = {"time_visibility": "hidden", "table_visiblity": "hidden",
                    "raw_output": {"file_url": "", "csv_url": ""}}
        return render_template('batch.html', result=response)


@app.route("/color/nearest", methods=['POST', 'GET'])
@nocache
def nearest_color():
    start_time = datetime.datetime.now()

    if request.method == 'POST':
        rgb = request.form['rgb']
        rgb = rgb.split(",")
        rgb = [int(i.strip()) for i in rgb]

        raw_output = color.get_nearest_color(rgb)

        end_time = datetime.datetime.now()
        duration = end_time - start_time
        response = {"raw_output": raw_output,
                    "time_visibility": "visible", "table_visiblity": "visible", "duration": duration}
        return render_template('nearest_color.html', result=response)

    else:
        rgb = [255, 255, 255]

        raw_output = color.get_nearest_color(rgb)

        end_time = datetime.datetime.now()
        duration = end_time - start_time
        response = {"raw_output": raw_output,
                    "time_visibility": "visible", "table_visiblity": "visible", "duration": duration}
        return render_template('nearest_color.html', result=response)


if app.config["DEBUG"]:
    @app.after_request
    def after_request(response):
        response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate, public, max-age=0"
        response.headers["Expires"] = -1
        response.headers["Pragma"] = "no-cache"
        return response
# prevent cached responses
if app.config["DEBUG"]:
    @app.before_request
    def before_request(response):
        response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate, public, max-age=0"
        response.headers["Expires"] = -1
        response.headers["Pragma"] = "no-cache"
        return response

if __name__ == "__main__":
    app.run(host=config.host, port=config.port, debug=config.DEBUG_MODE)
