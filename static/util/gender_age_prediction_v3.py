# import necessary packages
from pathlib import Path
import cv2
import dlib
import numpy as np
import argparse
from contextlib import contextmanager
from static.util.wide_resnet import WideResNet
from keras.utils.data_utils import get_file

pretrained_model = "https://github.com/yu4u/age-gender-estimation/releases/download/v0.5/weights.28-3.73.hdf5"
modhash = 'fbe63257a054c1c5466cfd7bf14646d6'

depth = 16
k = 8
weight_file = "/home/textmercato/filestore/age-gender-estimation/pretrained_models/weights.29-3.76_utk.hdf5"
margin = 0.4

# if not weight_file:
#     weight_file = get_file("weights.28-3.73.hdf5", pretrained_model, cache_subdir="pretrained_models",
#                            file_hash=modhash, cache_dir=str(Path(__file__).resolve().parent))

# for face detection
global detector
detector = dlib.get_frontal_face_detector()

# load model and weights
img_size = 64
# global gender_color_model
global gender_color_model


# t  ={"model":gender_color_model}

def draw_label(image, point, label, font=cv2.FONT_HERSHEY_SIMPLEX,
               font_scale=0.8, thickness=1):
    size = cv2.getTextSize(label, font, font_scale, thickness)[0]
    x, y = point
    cv2.rectangle(image, (x, y - size[1]), (x + size[0], y), (255, 0, 0), cv2.FILLED)
    cv2.putText(image, label, point, font, font_scale, (255, 255, 255), thickness, lineType=cv2.LINE_AA)


def yield_images_from_dir(image_dir):
    image_dir = Path(image_dir)

    for image_path in image_dir.glob("*.*"):
        img = cv2.imread(str(image_path), 1)

        if img is not None:
            h, w, _ = img.shape
            r = 640 / max(w, h)
            yield cv2.resize(img, (int(w * r), int(h * r)))


def process_image(image_path):
    img = cv2.imread(str(image_path), 1)
    h, w, _ = img.shape
    r = 640 / max(w, h)
    return cv2.resize(img, (int(w * r), int(h * r)))


# read input image


def predict(input_image, output_image, age_thresold = 15):
    gender_color_model = WideResNet(img_size, depth=depth, k=k)()
    gender_color_model.load_weights(weight_file)
    # image_generator = yield_images_from_dir(image_dir) if image_dir else yield_images()
    image_path = input_image
    image = process_image(image_path)
    # print(image)

    input_img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    img_h, img_w, _ = np.shape(input_img)

    # detect faces using dlib detector
    detected = detector(input_img, 1)
    faces = np.empty((len(detected), img_size, img_size, 3))
    all_ages = []
    all_genders = []

    if len(detected) > 0:
        print(detected)
        for i, d in enumerate(detected):
            x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
            xw1 = max(int(x1 - margin * w), 0)
            yw1 = max(int(y1 - margin * h), 0)
            xw2 = min(int(x2 + margin * w), img_w - 1)
            yw2 = min(int(y2 + margin * h), img_h - 1)
            cv2.rectangle(image, (x1, y1), (x2, y2), (255, 0, 0), 2)
            # cv2.rectangle(img, (xw1, yw1), (xw2, yw2), (255, 0, 0), 2)
            faces[i, :, :, :] = cv2.resize(image[yw1:yw2 + 1, xw1:xw2 + 1, :], (img_size, img_size))

        # predict ages and genders of the detected faces
        results = gender_color_model.predict(faces)
        predicted_genders = results[0]
        ages = np.arange(0, 101).reshape(101, 1)
        predicted_ages = results[1].dot(ages).flatten()

        # draw results
        for i, d in enumerate(detected):
            if predicted_genders[i][0] < 0.5:
                if int(predicted_ages[i]) <= age_thresold:
                    gender = "BOY"
                else:
                    gender = "MALE"
            else:
                if int(predicted_ages[i]) <= age_thresold:
                    gender = "GIRL"
                else:
                    gender = "FEMALE"
            label = "{}, {}".format(int(predicted_ages[i]),
                                    gender)

            try:
                all_ages.append(int(label.split(",")[0]))
                all_genders.append(label.split(",")[1])
            except Exception as e:
                pass
            draw_label(image, (d.left(), d.top()), label)
            
    print(all_ages)
    print(all_genders)
    cv2.imwrite(output_image, image)
    # save output
    # cv2.imwrite(output_image, img)

    # release resources
    # cv2.destroyAllWindows()
    # cv2.imshow("Detecting age and gender", resultImg)
    return {"message": "Fetched Result successfully.", "ages": all_ages, "genders": all_genders, "bbox": faces}
